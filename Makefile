.DEFAULT_GOAL := all
SHELL = /bin/sh

UID := $(shell id -u)
GID := $(shell id -g)

export UID
export GID

.PHONY: test
test: phpunit.xml composer.lock database
	docker-compose run --rm php vendor/bin/doctrine orm:schema-tool:update --force
	docker-compose run --rm php phpunit --colors --testdox --coverage-html ./build/coverage --coverage-clover ./build/clover.xml
# 	docker-compose run --rm php vendor/bin/coverage-check build/clover.xml 100

lint: composer.lock
	docker-compose run --rm php composer validate
	docker-compose run --rm php ./vendor/bin/php-cs-fixer fix ./src --dry-run --diff --using-cache no --ansi
	docker-compose run --rm php ./vendor/bin/phpstan analyze -l7 --no-progress  ./src --ansi

composer.lock:
	docker-compose run --rm php composer install

phpunit.xml:
	cp phpunit.xml.dist phpunit.xml

database:
	docker-compose up -d postgres
	docker-compose run --rm php ./wait-for-it.sh postgres:5432 -t 180 -- echo "Postgres is up"

.PHONY: clean
clean:
	-rm phpunit.xml
	-rm composer.lock
	docker-compose down

.PHONY: all
all: test clean
