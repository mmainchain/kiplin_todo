# The Kiplin Backend Technical Test (or KBTT)

Welcome to this test!

## Context
This is a todo-list application, which is using the hexagonal architecture.

If this is the first time you hear about this architecural pattern, it aims to separate domain concepts (related to the business) from infrastructure concerns (related to technical details).

The rules of this pattern are:
  - Code living in the `Domain` namespace MUST NOT depend on external system (filesystem, clock, network, ...)
  - Code living in the `Domain` namespace MUST NOT be context specific (can only run in a web or a cli context, for instance)
  - Code that requires the previous dependences MUST live in the `Infrastructure` namespace
  - `Domain` classes MUST NOT depend on `Infrastructure` classes
  - `Infrastructure` classes CAN depend on `Domain` classes

Furthermore, we have choosen to split read and write concerns (aka the CQRS pattern) by defining multiple repository interfaces. Check the `App\Domain\TodoRepository` and `App\Application\TodosRepository` to learn more about their contract.

## Running the app
For now, the app has no contact with the outside world (no shiny UI, no fancy responsive design, ...).

However, you may test your implementations thanks to carefully designed integration tests.

To launch the test suite, you need to run

```
$ make test
```

## Mission
Your mission, should you decide to accept it, is to implement a read and a write repository with the backend storage technology of your choice. It could be, but is not limited to, a RDBMS. You may implement both interfaces within the same class.

We provide you with an in-memory implementation, as an example!

You will use git to log the history of your implementation.

The testsuite must pass by simply running `make all`.

You will add a README explaining your storage choice, along with any modifications or suggestions you have or would have done to the codebase.

This whole test should not last more than one hour. We recommend to document any extra work you would have performed.

At the end of the exercise, archive your project, including the `.git` directory and excluding the `vendor` directory.

You may now send it to your Kiplin contact, and we will get in touch soon!

Thanks!

## Implementation

### Relational DataBase Management System

First of all, I added the RDBMS to the project as a PostgreSQL container, for a simple reason: PostgreSQL is the RDBMS that I use on a daily basis in my current position.
I could have chosen MySQL as I use to know it way before working with PostgreSQL. Both are very similar when a simple relational database is needed without other complex requirements.

> PostgreSQL could allow you to persist Todos as JSON values, but I decided to keep it as a classical relational persistence.

I added it as a container in the `docker-compose.yml` file.

So it could fully work in the project layout, I had to start it before testing. This implies that postgres has to be up when the project tests are run, so I tricked it this way:

  - I had a `database` target in the Makefile in order to start the container ;
  - I added a [`wait-for-it.sh`](https://github.com/vishnubob/wait-for-it/blob/master/wait-for-it.sh) script in order to wait for postgres to be up before executing anything else ;

This results as the following target:

```
database:
	docker-compose up -d postgres
	docker-compose run --rm php ./wait-for-it.sh postgres:5432 -t 180 -- echo "Postgres is up"
```

### Entity mapping

I decided to use [Doctrine ORM](https://www.doctrine-project.org/projects/orm.html) to transform my PHP model into a relational one for the following reasons:

  - It simplifies a lot the infrastructure-related code thanks to its abstract layer ;
  - In this case, it prevents from using native SQL queries in repositories, and from all well-known related problems ;
  - We can abstract the database model and concentrate on our codebase

So I implemented my model as a Doctrine Entity in the `Infrastructure` namespace: `Todo.php`.

> We create the model thanks to doctrine CLI in the test target of the makefile, just before running tests, thanks to the following command line: `docker-compose run --rm php vendor/bin/doctrine orm:schema-tool:update --force`

### Hexagonal architecture

The `Infrastructure\Todo` entity allows me to separate the domain and the infrastructure. I developed the following functions to preserve the pattern:

  - A factory used to create the Infrastructure Entity from the domain Todo (**write** use case):
```php
# App\Infrastructure\Entity\Todo
public static function fromDomain(DomainTodo $todo): InfrastructureTodo
```

  - A function that enables Domain entity instanciation from a Infrastructure entity (**read** use case):
```php
# App\Infrastructure\Entity\Todo
public function toDomain(): DomainTodo
```

  - A function that allows an Infrastructure Todo to update from a Domain one (**write** use case):
```php
# App\Infrastructure\Entity\Todo
public function apply(DomainTodo $todo): InfrastructureTodo
```

  - A function that can instanciate the Application OpenedTodo entity from the Infrastructure one (**read** use case):
```php
# App\Infrastructure\Entity\Todo
public function toOpenedTodo(): OpenedTodo
```

### DatabaseTodoRepository

Thanks to Doctrine Entity Mapping and functions added in Infrastructure side `Todo`, I implemented the `DatabaseTodoRepository` using Doctrine's Entity Manager and Repositories functions.

All I had to do was to extend this Repository from Doctrine's `EntityRepository`:

```php
# App\Infrastructure\Repository\DatabaseTodoRepository
class DatabaseTodoRepository extends EntityRepository implements TodosRepository, TodoRepository
```

And link it to the related Doctrine Entity:

```php
# App\Infrastructure\Entity\Todo

/** @ORM\Entity(repositoryClass="App\Infrastructure\Repository\DatabaseTodoRepository") */
class Todo
```

### Existing test cases

The existing test classes `App\Tests\Integration\ReadRepository\TodosRepositoryTest` and `App\Tests\Integration\WriteRepository\TodoRepositoryTest` have been tricked to allow the doctrine context in it thanks to a new `InfrastructureTestCase` that encapsulate Doctrine logic in it:

```php
# App\Tests\Integration\InfrastructureTestCase
class InfrastructureTestCase extends TestCase
{
    public function initializeDoctrine(): EntityManagerInterface
    protected function getEntityManager(): EntityManagerInterface
}
```

Thanks to this, all I had to do was to extend existing test classes from this `InfrastructureTestCase`, and add the `DatabaseTodoRepository` in the data provider functions.

### Extra work

#### Improved test cases

I decided to write new test cases alongside the existing ones, to test the same things but with a different method.

The method I used to build my test cases is based on:

  - [Data test builders](https://www.javacodegeeks.com/2013/06/builder-pattern-good-for-code-great-for-tests.html) which allows encapsulating non-tested logic as well as reducing the amount of code written in a single test and making it easy to read
  - Single tested feature: `itFindsExistingTodo`, `itDoesNotFindNonExistingTodo`, `itFindsOpenedTodos`...
  - Deport huge assertions in order to have readable tests and to factorize it for different tests (e.g. `assertThatArrayContainsTodo()`)

I like to work using the Test-first method, so I wrote those tests just before I wrote my code.

On a daily basis I work with TDD and would love to expand my knowledge on these methods as I'm still learning :)

#### Code quality and coverage

Alongside my professional and personal projects, I work with code quality tools. So for this test I decided to add the following:

  - PHPStan (PHP Static Analysis)
  - CS-fixer (PHP Coding standards)
  - Xdebug (For code coverage purpose)

I had to add the first two ones in composer dev dependencies, and add the third one in the PHP Dockerfile.

I also added a `lint` makefile target using this tools:

```
lint: composer.lock
	docker-compose run --rm php composer validate
	docker-compose run --rm php ./vendor/bin/php-cs-fixer fix ./src --dry-run --diff --using-cache no --ansi
	docker-compose run --rm php ./vendor/bin/phpstan analyze -l7 --no-progress  ./src --ansi
```

The code coverage was to make sure the newly added code was totally tested. So I changed the `test` makefile target to generate the code coverage:

```
test: phpunit.xml composer.lock database
	docker-compose run --rm php vendor/bin/doctrine orm:schema-tool:update --force
	docker-compose run --rm php phpunit --colors --testdox --coverage-html ./build/coverage --coverage-clover ./build/clover.xml
```

This generates the code coverage as an interactive static HTML page set in the `./build/coverage` directory, as well as a code coverage reporting in the `./build/clover.xml` file.

I use to use a composer dependency tool checking my code coverage as a command line: `rregeer/phpunit-coverage-check` and add it in the test target:

```
test: phpunit.xml composer.lock database
	docker-compose run --rm php vendor/bin/doctrine orm:schema-tool:update --force
	docker-compose run --rm php phpunit --colors --testdox --coverage-html ./build/coverage --coverage-clover ./build/clover.xml
# 	docker-compose run --rm php vendor/bin/coverage-check build/clover.xml 100
```

> It stays commented to allow `make test` target to run correctly. However, it fails as the code coverage is not 100% here.