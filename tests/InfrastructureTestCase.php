<?php

namespace App\Tests;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;

use PHPUnit\Framework\TestCase;

class InfrastructureTestCase extends TestCase
{
    private ?EntityManagerInterface $entityManager;

    public function initializeDoctrine(): EntityManagerInterface
    {
        $config = Setup::createAnnotationMetadataConfiguration(["/../src/Infrastructure/Entity"], true, null, null, false);

        $connectionParams = [
            'dbname' => 'todo',
            'user' => 'todo',
            'host' => 'postgres',
            'driver' => 'pdo_pgsql',
        ];

        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
        $entityManager = EntityManager::create($conn, $config);
        $entityManager->getConnection()->beginTransaction();

        $this->entityManager = $entityManager;

        return $entityManager;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager ?? $this->initializeDoctrine();
    }
}