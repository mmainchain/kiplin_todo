<?php

namespace App\Tests\Unit\Infrastructure\Entity;

use App\Infrastructure\Entity\Todo;
use App\Tests\Fixtures\Domain\TodoBuilder as DomainTodoBuilder;
use App\Tests\Fixtures\Infrastructure\TodoBuilder;
use PHPUnit\Framework\TestCase;

class TodoTest extends TestCase
{
    /**
     * @test
     */
    public function itBuildsInfrastructureTodoFromDomain(): void
    {
        $domainTodo = DomainTodoBuilder::for($this)->any();

        $infrastructureTodo = Todo::fromDomain($domainTodo)->toDomain();

        $this->assertEquals($domainTodo, $infrastructureTodo);
    }

    /**
     * @test
     */
    public function itApplyDomainTodoToInfrastructureTodo(): void
    {
        /** @var Todo $todo */
        $todo = TodoBuilder::for($this)
            ->withId('08074af8-10ea-4c93-bd7f-15873dfa86fc')
            ->withDescription('Buy milk')
            ->buildOpened();

        $domainTodo = DomainTodoBuilder::for($this)
            ->withId('08074af8-10ea-4c93-bd7f-15873dfa86fc')
            ->withDescription('Buy sugar')
            ->buildClosed();

        $appliedTodo = $todo->apply($domainTodo)->toDomain();

        ['id' => $id, 'description' => $description, 'status' => $status] = $appliedTodo->toData();
        $this->assertEquals('08074af8-10ea-4c93-bd7f-15873dfa86fc', $id);
        $this->assertEquals('Buy sugar', $description);
        $this->assertEquals('closed', $status);
    }

    /**
     * @test
     */
    public function itBuildsDomainTodoFromInfrastructure(): void
    {
        /** @var Todo $todo */
        $todo = TodoBuilder::for($this)
            ->withId('08074af8-10ea-4c93-bd7f-15873dfa86fc')
            ->withDescription('Buy milk')
            ->buildOpened();

        $domainTodo = $todo->toDomain();

        ['id' => $id, 'description' => $description, 'status' => $status] = $domainTodo->toData();
        $this->assertEquals('08074af8-10ea-4c93-bd7f-15873dfa86fc', $id);
        $this->assertEquals('Buy milk', $description);
        $this->assertEquals('opened', $status);
    }

    /**
     * @test
     */
    public function itBuildsOpenedTodoFromInfrastructure(): void
    {
        /** @var Todo $todo */
        $todo = TodoBuilder::for($this)
            ->withId('08074af8-10ea-4c93-bd7f-15873dfa86fc')
            ->withDescription('Buy milk')
            ->buildOpened();

        $domainTodo = $todo->toOpenedTodo();

        $this->assertEquals('08074af8-10ea-4c93-bd7f-15873dfa86fc', $domainTodo->id);
        $this->assertEquals('Buy milk', $domainTodo->description);
    }
}