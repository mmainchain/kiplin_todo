<?php
declare(strict_types=1);

namespace App\Tests\Integration\WriteRepository;

use App\Domain\CannotCloseTodo;
use App\Domain\Todo;
use App\Domain\TodoDescription;
use App\Domain\TodoId;
use App\Domain\TodoRepository;
use App\Infrastructure\Repository\DatabaseTodoRepository;
use App\Infrastructure\Repository\InMemoryTodoRepository;
use App\Tests\InfrastructureTestCase;
use App\Infrastructure\Entity\Todo as InfrastructureTodo;

final class TodoRepositoryTest extends InfrastructureTestCase
{
    /**
     * @dataProvider provideConcretions
     */
    public function testTodoPersistence(TodoRepository $repository): void
    {
        $id = TodoId::generate();

        $todo = Todo::open($id, TodoDescription::fromString('Buy milk'));
        $repository->save($todo);

        $this->assertNull($repository->get(TodoId::generate()));

        $todo = $repository->get($id);
        $this->assertInstanceOf(Todo::class, $todo);
        $todo->close();
        $repository->save($todo);

        try {
            $todo = $repository->get($id);
            $todo->close();
        } catch (CannotCloseTodo) {
            return;
        }

        throw new \RuntimeException('Expecting to not be able to close already closed todo.');
    }

    public function provideConcretions(): \Generator
    {
        yield InMemoryTodoRepository::class => [new InMemoryTodoRepository()];
        yield DatabaseTodoRepository::class => [$this->getEntityManager()->getRepository(InfrastructureTodo::class)];
    }
}
