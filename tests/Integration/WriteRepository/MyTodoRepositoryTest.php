<?php

namespace App\Tests\Integration\WriteRepository;

use App\Domain\TodoRepository;
use App\Infrastructure\Entity\Todo as InfrastructureTodo;
use App\Infrastructure\Repository\InMemoryTodoRepository;
use App\Infrastructure\Repository\DatabaseTodoRepository;
use App\Tests\Fixtures\Domain\TodoBuilder;
use App\Tests\InfrastructureTestCase;

class MyTodoRepositoryTest extends InfrastructureTestCase
{
    /**
     * @test
     * @dataProvider provideConcretions
     */
    public function itSaveNonExistingTodo(TodoRepository $repository): void
    {
        $todo = TodoBuilder::for($this)->any();

        $repository->save($todo);

        $this->assertEquals($todo, $repository->get($todo->id()));
    }

    /**
     * @test
     * @dataProvider provideConcretions
     */
    public function itSaveExistingTodo(TodoRepository $repository): void
    {
        $todo = TodoBuilder::for($this)->any();

        $repository->save($todo);
        $repository->save($todo);

        $this->assertEquals($todo, $repository->get($todo->id()));
    }

    public function provideConcretions(): \Generator
    {
        yield InMemoryTodoRepository::class => [new InMemoryTodoRepository()];
        yield DatabaseTodoRepository::class => [$this->getEntityManager()->getRepository(InfrastructureTodo::class)];
    }
}
