<?php
declare(strict_types=1);

namespace App\Tests\Integration\ReadRepository;

use App\Application\ReadModel\TodosRepository;
use App\Domain\Todo;
use App\Domain\TodoDescription;
use App\Domain\TodoId;
use App\Domain\TodoRepository;
use App\Infrastructure\Repository\DatabaseTodoRepository;
use App\Infrastructure\Repository\InMemoryTodoRepository;
use App\Tests\InfrastructureTestCase;
use App\Infrastructure\Entity\Todo as InfrastructureTodo;

final class TodosRepositoryTest extends InfrastructureTestCase
{
    /**
     * @dataProvider provideConcretions
     */
    public function testListOpenedTodos(
        TodoRepository $writeModelRepository,
        TodosRepository $readModelRepository,
        ?callable $setuper = null
    ): void {
        if ($setuper) {
            $setuper();
        }

        $writeModelRepository->save($this->openedTodo());
        $writeModelRepository->save($this->openedTodo());
        $writeModelRepository->save($this->closedTodo());
        $writeModelRepository->save($this->openedTodo());
        $writeModelRepository->save($this->closedTodo());
        $writeModelRepository->save($this->closedTodo());

        $this->assertCount(3, $readModelRepository->opened());
    }

    public function provideConcretions(): \Generator
    {
        $inMemoryRepository = new InMemoryTodoRepository();
        yield InMemoryTodoRepository::class => [$inMemoryRepository, $inMemoryRepository];

        $postgresRepository = $this->getEntityManager()->getRepository(InfrastructureTodo::class);
        yield DatabaseTodoRepository::class => [$postgresRepository, $postgresRepository];
    }

    private function openedTodo(): Todo
    {
        return Todo::open(TodoId::generate(), TodoDescription::fromString('Buy milk'));
    }

    private function closedTodo(): Todo
    {
        $todo = $this->openedTodo();
        $todo->close();

        return $todo;
    }
}
