<?php

namespace App\Tests\Integration\ReadRepository;

use App\Application\ReadModel\OpenedTodo;
use App\Domain\TodoId;
use App\Domain\TodoRepository;
use App\Domain\Todo;
use App\Infrastructure\Entity\Todo as InfrastructureTodo;
use App\Infrastructure\Repository\InMemoryTodoRepository;
use App\Infrastructure\Repository\DatabaseTodoRepository;
use App\Tests\Fixtures\Domain\TodoBuilder;
use App\Tests\InfrastructureTestCase;

class MyTodoRepositoryTest extends InfrastructureTestCase
{
    /**
     * @test
     * @dataProvider provideConcretions
     */
    public function itFindsExistingTodo(TodoRepository $repository): void
    {
        $todo = TodoBuilder::for($this)->any();
        $repository->save($todo);

        $persistedTodo = $repository->get($todo->id());

        $this->assertEquals($todo, $persistedTodo);
    }

    /**
     * @test
     * @dataProvider provideConcretions
     */
    public function itDoesNotFindNonExistingTodo(TodoRepository $repository): void
    {
        $repository->save(TodoBuilder::for($this)->any());

        $nonExistingTodoId = TodoId::generate();

        $this->assertNull($repository->get($nonExistingTodoId));
    }

    /**
     * @test
     * @dataProvider provideConcretions
     */
    public function itFindsOpenedTodos(TodoRepository $repository): void
    {
        $openedTodo = TodoBuilder::for($this)->buildOpened();

        $repository->save($openedTodo);

        $openedTodos = $repository->opened();

        $this->assertThatArrayContainsTodo($openedTodos, $openedTodo);
    }

    /**
     * @test
     * @dataProvider provideConcretions
     */
    public function itDoesNotFindsClosedTodos(TodoRepository $repository): void
    {
        $closedTodo = TodoBuilder::for($this)->buildClosed();

        $repository->save($closedTodo);

        $openedTodos = $repository->opened();

        $this->assertThatArrayDoesNotContainsTodo($openedTodos, $closedTodo);
    }

    public function provideConcretions(): \Generator
    {
        yield InMemoryTodoRepository::class => [new InMemoryTodoRepository()];
        yield DatabaseTodoRepository::class => [$this->getEntityManager()->getRepository(InfrastructureTodo::class)];
    }

    private function assertThatArrayContainsTodo(array $actualTodos, Todo $expectedTodo): void
    {
        /** @var OpenedTodo $actualTodo */
        foreach ($actualTodos as $actualTodo) {
            $this->assertInstanceOf(OpenedTodo::class, $actualTodo);

            ['id' => $id, 'description' => $description] = $expectedTodo->toData();

            $consistency = $actualTodo->id === $id;
            $consistency &= $actualTodo->description === $description;

            if ($consistency) {
                $this->assertTrue(true);

                return;
            }
        }

        $this->fail('Fail asserting that given todo is in array.');
    }

    private function assertThatArrayDoesNotContainsTodo(array $actualTodos, Todo $expectedTodo): void
    {
        /** @var OpenedTodo $actualTodo */
        foreach ($actualTodos as $actualTodo) {
            $this->assertInstanceOf(OpenedTodo::class, $actualTodo);

            ['id' => $id, 'description' => $description] = $expectedTodo->toData();

            $consistency = $actualTodo->id === $id;
            $consistency &= $actualTodo->description === $description;

            if ($consistency) {
                $this->fail('Fail asserting that given todo is not in array.');

                return;
            }
        }

        $this->assertTrue(true);
    }
}
