<?php

namespace App\Tests\Fixtures\Domain;

use App\Domain\Todo;
use App\Tests\Fixtures\AbstractBuilder;
use Symfony\Component\Uid\Uuid;

class TodoBuilder extends AbstractBuilder
{
    private const STATUS_OPENED = 'opened';
    private const STATUS_CLOSED = 'closed';

    private ?string $id = null;
    private ?string $description = null;
    private ?string $status = null;

    public function withId(string $id): TodoBuilder
    {
        $this->id = $id;

        return $this;
    }

    public function withDescription(string $description): TodoBuilder
    {
        $this->description = $description;

        return $this;
    }

    public function buildOpened(): Todo
    {
        $this->status = self::STATUS_OPENED;

        return $this->build();
    }

    public function buildClosed(): Todo
    {
        $this->status = self::STATUS_CLOSED;

        return $this->build();
    }

    public function build(): Todo
    {
        return Todo::fromData([
            'id' => $this->id ?? Uuid::v4()->__toString(),
            'description' => $this->description ?? (string) rand(),
            'status' => $this->status ?? self::STATUS_OPENED,
        ]);
    }

    public function clear(): AbstractBuilder
    {
        $this->id = null;
        $this->description = null;
        $this->status = null;

        return $this;
    }
}
