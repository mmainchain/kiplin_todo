<?php

namespace App\Tests\Fixtures;

use PHPUnit\Framework\TestCase;

abstract class AbstractBuilder
{
    protected TestCase $testCase;

    public static function for(TestCase $testCase): AbstractBuilder
    {
        return new static($testCase);
    }

    private function __construct(TestCase $testCase)
    {
        $this->testCase = $testCase;
    }

    public function any()
    {
        return $this->clear()->build();
    }

    public abstract function build();

    public abstract function clear(): AbstractBuilder;
}
