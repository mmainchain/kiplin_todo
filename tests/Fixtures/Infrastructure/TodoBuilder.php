<?php

namespace App\Tests\Fixtures\Infrastructure;

use App\Domain\TodoStatus;
use App\Infrastructure\Entity\Todo;
use App\Tests\Fixtures\AbstractBuilder;
use Symfony\Component\Uid\Uuid;

class TodoBuilder extends AbstractBuilder
{
    private ?string $id = null;
    private ?string $description = null;
    private ?string $status = null;

    public function withId(string $id): TodoBuilder
    {
        $this->id = $id;

        return $this;
    }

    public function withDescription(string $description): TodoBuilder
    {
        $this->description = $description;

        return $this;
    }

    public function buildOpened(): Todo
    {
        $this->status = TodoStatus::opened()->asString();

        return $this->build();
    }

    public function buildClosed(): Todo
    {
        $this->status = TodoStatus::closed()->asString();

        return $this->build();
    }

    public function build(): Todo
    {
        return new Todo(
            $this->id ?? Uuid::v4()->__toString(),
            $this->description ?? (string) rand(),
            $this->status ?? TodoStatus::opened()->asString(),
        );
    }

    public function clear(): AbstractBuilder
    {
        $this->id = null;
        $this->description = null;
        $this->status = null;

        return $this;
    }
}
