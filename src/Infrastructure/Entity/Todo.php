<?php

namespace App\Infrastructure\Entity;

use App\Application\ReadModel\OpenedTodo;
use Doctrine\ORM\Mapping as ORM;
use App\Domain\Todo as DomainTodo;

/** @ORM\Entity(repositoryClass="App\Infrastructure\Repository\DatabaseTodoRepository") */
class Todo
{
    /** @ORM\Id @ORM\Column(type="string") */
    protected string $id;
    /** @ORM\Column(type="string") */
    protected string $description;
    /** @ORM\Column(type="string") */
    protected string $status;

    public function __construct(string $id, string $description, string $status)
    {
        $this->id = $id;
        $this->description = $description;
        $this->status = $status;
    }

    public static function fromDomain(DomainTodo $todo): Todo
    {
        ['id' => $id, 'description' => $description, 'status' => $status] = $todo->toData();

        return new self($id, $description, $status);
    }

    public function apply(DomainTodo $todo): Todo
    {
        ['description' => $description, 'status' => $status] = $todo->toData();

        $this->description = $description;
        $this->status = $status;

        return $this;
    }

    public function toDomain(): DomainTodo
    {
        return DomainTodo::fromData([
            'id' => $this->id,
            'description' => $this->description,
            'status' => $this->status,
        ]);
    }

    public function toOpenedTodo(): OpenedTodo
    {
        $openedTodo = new OpenedTodo();
        $openedTodo->id = $this->id;
        $openedTodo->description = $this->description;

        return $openedTodo;
    }
}
