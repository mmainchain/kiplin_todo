<?php

namespace App\Infrastructure\Repository;

use App\Application\ReadModel\TodosRepository;
use App\Domain\Todo as DomainTodo;
use App\Domain\TodoId;
use App\Domain\TodoRepository;
use App\Domain\TodoStatus;
use App\Infrastructure\Entity\Todo;
use Doctrine\ORM\EntityRepository;

class DatabaseTodoRepository extends EntityRepository implements TodosRepository, TodoRepository
{
    public function get(TodoId $id): ?DomainTodo
    {
        /** @var ?Todo $todo */
        $todo = $this->find($id->asString());

        return $todo ? $todo->toDomain() : null;
    }

    public function save(DomainTodo $domainTodo): void
    {
        $todo = $this->find($domainTodo->id()->asString()) ?? Todo::fromDomain($domainTodo);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($todo->apply($domainTodo));
        $entityManager->flush();
    }

    public function opened(): array
    {
        $todos = $this->findBy(['status' => TodoStatus::opened()->asString()]) ?? [];

        return array_map(fn (Todo $todo) => $todo->toOpenedTodo(), $todos);
    }
}
